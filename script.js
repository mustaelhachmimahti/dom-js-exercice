const allFunctions = _ => {
  const title = document.getElementsByClassName("title");
  for (let i = 0; i < title.length; i++) {
    const foo = title[i].innerHTML;
    console.log("Premier H1 -> Contenu", foo);
  }

  const menu = document.querySelector("ul").firstElementChild.textContent;
  const menu2 = document.querySelector("ul").firstElementChild.innerHTML;
  const menu3 =
    document.getElementById("menu").childNodes[1].firstElementChild.innerHTML;

  console.log("first LI:", menu, document.querySelector("ul span"));
  console.log("first LI:", menu2);
  console.log("first LI:", menu3);

  const menu4 = document.querySelector("ul").lastElementChild.innerHTML;

  console.log("last LI:", menu4);

  const menu5 = document.querySelector("ul li:nth-child(3)").innerHTML;

  console.log("Troisième LI:", menu5);

  const menu6 = document.querySelector("li").parentNode.innerHTML;

  console.log("HTML du parent de : <ul>", menu6, "</ul>");

  console.log("HTML du parent de <ul>");
  const forBucle = document.querySelectorAll("ul li");
  for (let i = 1; i <= forBucle.length; i++) {
    const menu6 = document.querySelector(`ul li:nth-child(${i})`).innerHTML;
    console.log("<li>", menu6, "</li>");
  }
};

window.addEventListener("load", allFunctions);

const colorChangeRed = _ => {
  setTimeout(function () {
    document.querySelector("ul").style.color = "Red";
  }, 200);
};
const colorChangeBlack = _ => {
  setTimeout(function () {
    document.querySelector("ul").style.color = "Black";
  }, 200);
};

document.querySelector("ul").addEventListener("mouseover", colorChangeRed);
document.querySelector("ul").addEventListener("mouseout", colorChangeBlack);
